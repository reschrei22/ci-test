FROM php:5.6-apache

ADD . /var/www/html

RUN docker-php-ext-install -j$(nproc) mysql && a2enmod rewrite